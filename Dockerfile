ARG BASE_IMG

FROM ${BASE_IMG}

SHELL ["/bin/bash", "-ci"]
ENV DEBIAN_FRONTEND noninteractive

WORKDIR /workspace/ros_ws

RUN apt-get update && apt-get install -y \
    ros-noetic-realsense2-camera ros-noetic-realsense2-description \
    ros-noetic-usb-cam && \
    rm -rf /var/lib/apt/lists/*

CMD ["/bin/bash", "-ci", "realsense-viewer"]